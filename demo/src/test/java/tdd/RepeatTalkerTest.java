package tdd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RepeatTalkerTest 
{
    @Test
    public void talkCorrectOuput()
    {
        RepeatTalker repeatTalker = new RepeatTalker(3);

        assertEquals(repeatTalker.talk("hello"),"hello hello hello");
    }
}
