package tdd;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ShiftedTalkerTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void nTooHigh()
    {
        try {
            ShiftedTalker st1 = new ShiftedTalker(5);
            st1.talk("sup");
            assertTrue( false );
        }
        catch (Exception e)
        {
            assertTrue( true );
        }
    }

    @Test
    public void negativeInput()
    {
        try {
            ShiftedTalker st2 = new ShiftedTalker(-1);
            assertTrue( false );
        }
        catch (Exception e)
        {
            assertTrue( true );
        }
    }
    
    @Test
    public void wrongOutput()
    {
        ShiftedTalker st3 = new ShiftedTalker(3);
        assertEquals(st3.talk("abcdef"), "defabc");
    }
}